import * as _ from 'lodash';
import {validate, Length, Matches, IsInt, Min, Max, MinLength, IsEnum, ValidationError} from "class-validator";


export interface IUser  {
  first_name: string;
  last_name: string;
  active: boolean;
  age: number;
  login: string;
  password: string;
  role: number;
  registred_on: number;
  validate(): Promise<string []>;
  fullName(): string;
  register(): IUser;
}

export enum UserRoles {
  Administrator = 1, 
  Technician = 2, 
  Manager = 3, 
  Supervisor = 4, 
}

export class User implements IUser {
  @Length(3, 15, { 
    message: "From $constraint1 to  $constraint2 characters, but actual is $value"
  })
  public first_name: string;

  @Length(3, 25, { 
    message: "From $constraint1 to  $constraint2 characters, but actual is $value"
  })
  public last_name: string;

  public active: boolean;

  @IsInt()
  @Min(18, {
    message: "You are too young: minimum age is $constraint1, but you are $value"
  })
  @Max(55, {
    message: "You are too old: max age is $constraint1, but you are $value"
  })
  public age: number;

  @MinLength(8, {
    message: "Login min length is $constraint1"
  })
  @Matches(/^[a-z0-9\-_]+$/, {
    message: "Forbidden symbols: only a-z, 0-9 or '-' or '_"
  })
  public login: string;

  @MinLength(8,  {
    message: "Password min length is $constraint1"
  })
  public password: string;

  @IsEnum(UserRoles, {message: "Impossible role"})
  public role: number;

  public registred_on: number;

  /** METHODS **/

  public static create(obj?: IUser): User {
    const newUser: User = new User();
    return Object.assign(newUser, obj || {});
  }
  /* Эмуляция сохрания модели, присваиваем ей register_on
  */
  public register(): IUser{
    this.registred_on = this.registred_on || new Date().getTime();
    return this;
  }


  public fullName() {
    return `${this.first_name || ""} ${this.last_name || ""}`;
  }

  public validate(): Promise<any> {
    return validate(this).then((errors: ValidationError[]) => {
      return _.map(errors, (error) => {
        return _.pick(error, ['property', 'constraints'])
      });
    });
  }
};

