import * as React from 'react';
import { User, IUser } from "../../models/User";
import * as _ from 'lodash';
import { TextField, MenuItem, Paper, Switch, FormControlLabel, Button } from 'material-ui';
import './UserCard.css';


interface IUserCardProps {
  user: IUser,
  mode?: string,
  cancelMethod(): any,
  successMethod(user: IUser): any,

}

interface IUserCardState {
  user: IUser,
  errors: any,
  submitted: boolean,
  mode?: string,
}

const roles: { value: number, label: string }[] = [
  {
    value: 1,
    label: 'Administrator',
  },
  {
    value: 2,
    label: 'Technician',
  },
  {
    value: 3,
    label: 'Manager',
  },
  {
    value: 4,
    label: 'Supervisor',
  },
];



class UserCard extends React.Component<IUserCardProps, IUserCardState> {
  constructor(props?: IUserCardProps, context?: any) {
    super(props, context);
    const mode = this.props.mode || (this.props.user.registred_on ? 'edit' : 'new')
    this.state = {mode, user: User.create(this.props.user), submitted: false, errors: null};
  }

  handleChange = (property: string) => async (event: any, value?: any) => {
    const user = _.clone(this.state.user);
    switch (typeof user[property]) {
      case 'number':
        value = value || parseInt(event.target.value);
        break;
      case 'boolean':
        //значение уже будет boolean
        break;
      case 'string':
      default: 
        value = event.target.value;
    }
    _.set(user, property, value);
    const errors = await user.validate();
    if (this.state.submitted || this.state.mode === 'edit') {
      this.setState({user, errors});
    } else {
      this.setState({user});
    }
  };

  getError(property: string) {
    const error = _.find(this.state.errors, {property});
    return _.head(_.values(_.result(error, 'constraints')));
  }

  propValid(property: string) {
    return _.isEmpty(this.getError(property));
  }

  updateUser = async() =>{

    let errors = await this.state.user.validate();

    if (_.isEmpty(errors)) {
      errors = this.props.successMethod(this.state.user);
      if (_.isEmpty(errors)) {
        this.props.cancelMethod();
        return;
      }
    }
    this.setState({ errors, submitted: true });
    
  }

  renderActions() {
    const { cancelMethod } = this.props;
    const activeBtn = (this.state.mode === 'edit') ?
      <Button color="primary" className="UserCard__action" onClick={this.updateUser}>Save</Button> :
      <Button color="primary" className="UserCard__action" onClick={this.updateUser}>Create</Button>;

    return (
      <div className="UserCard__actions"> 
        <Button className="UserCard__action" onClick={cancelMethod}>Cancel</Button>
        {activeBtn}
      </div>
    );
  }

  render() {
    return (
      <Paper className="UserCard">
        <div className="UserCard__header"> User {this.state.user.fullName()}</div>
        <form className="UserCard__container" noValidate autoComplete="off">

          <FormControlLabel
            label={this.state.user.active ? 'Active' : 'Not active'}
            control={
              <Switch
                checked={this.state.user.active}
                onChange={this.handleChange('active')}
              />
            }
          />
          <TextField
            id="first_name"
            label="First Name"
            className="UserCard__field"
            value={this.state.user.first_name} 
            onChange={this.handleChange('first_name')}
            error={!this.propValid('first_name')}
            helperText={this.getError('first_name')}
            margin='normal'
          />
          <TextField
            id="last_name"
            label="Last Name"
            className="UserCard__field"
            onChange={this.handleChange('last_name')}
            value={this.state.user.last_name}
            error={!this.propValid('last_name')}
            helperText={this.getError('last_name')}
            margin='normal'
          />
          <TextField
            id="login"
            label="Login"
            className="UserCard__field"
            onChange={this.handleChange('login')}
            value={this.state.user.login}
            error={!this.propValid('login')}
            helperText={this.getError('login')}
            margin='normal'
          />
          <TextField
            id="password"
            label="Password"
            type="password"
            className="UserCard__field"
            onChange={this.handleChange('password')}
            value={this.state.user.password}
            error={!this.propValid('password')}
            helperText={this.getError('password')}
            margin='normal'
          />
          <TextField
            id="age"
            type="number"
            label="Age"
            className="UserCard__field"
            onChange={this.handleChange('age')}
            value={this.state.user.age}
            error={!this.propValid('age')}
            helperText={this.getError('age')}
            margin='normal'
          />
          <TextField
            id="role"
            select
            label="Role"
            className="UserCard__field"
            value={this.state.user.role || 0}
            margin='normal'
            onChange={this.handleChange('role')}
            error={!this.propValid('role')}
            SelectProps={{
              MenuProps: {
                className: "UserCard__role-select",
              },
            }}
            helperText={this.getError('role')}
          >
            {roles.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>

          {this.renderActions()}
        </form>
      </Paper>
    );
  }
}

export default UserCard;
