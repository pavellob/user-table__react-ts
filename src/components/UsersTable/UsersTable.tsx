import * as React from 'react';
import * as _ from 'lodash';
import * as moment from 'moment';
import { User, IUser, UserRoles } from "../../models/User";
import Table, { TableBody, TableCell, TableHead, TableRow, TableSortLabel } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Tooltip from 'material-ui/Tooltip';

const columnData = [
  { id: 'role', numeric: false, disablePadding: false, label: 'Role' },
  { id: 'login', numeric: false, disablePadding: true, label: 'Login' },
  { id: 'first_name', numeric: false, disablePadding: false, label: 'Full Name' },
  { id: 'age', numeric: true, disablePadding: false, label: 'Age' },
  { id: 'registred_on', numeric: true, disablePadding: false, label: 'Registred on' },
  { id: 'active', numeric: false, disablePadding: false, label: 'Active' },
];

class EnhancedTableHead extends React.Component<any, any> {

  constructor(props?: any, context?: any) {
    super(props, context);
    console.log(props)
  }

  createSortHandler = (property:any) => (event:any) => {
    this.props.onRequestSort(event, property);
  };


  render() {
    const { order, orderBy } = this.props;
    return (
      <TableHead>
        <TableRow>
          {columnData.map((column: any) => {
              return (
                <TableCell key={column.id}>
                  <Tooltip
                    title="Sort"
                    placement='bottom-start'
                    enterDelay={300}
                  >
                    <TableSortLabel
                      active={orderBy === column.id}
                      direction={order}
                      onClick={this.createSortHandler(column.id)}
                    >
                      {column.label}
                    </TableSortLabel>
                  </Tooltip>
                </TableCell>
              );
            }, this)}
        </TableRow>
      </TableHead>
    );
  }
}



const userRoles = _.map(UserRoles, (value, key) => {
  return {role: key, roleCode: value};
});


const prepareDate = (users: any): Array<User> => {
  const data = _.map(users, user => User.create(user as IUser));
  return _.orderBy(data, ['login', 'active'], ['asc', 'desc']);
}

export default class UsersTable extends React.Component<any, any>  {
  constructor(props?: any, context?: any) {
    super(props, context);
    this.state = {
      order: 'asc',
      orderBy: 'login',
      data: prepareDate(props.users),
    };
  }


  getRole(roleCode: number) {
    return _.result(_.find(userRoles, {roleCode}), 'role', roleCode);
  }

  getTime(registred_on: number) {
    return moment(registred_on).format("DD.MM.YYYY HH:mm");
  }

  handleRequestSort = (event: any, property: any) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data =  _.orderBy(this.state.data, [orderBy, 'active'], [order, 'desc']);

    this.setState({ data, order, orderBy });
  };



  componentWillReceiveProps (props: any) {
    const data = prepareDate(props.users);
    console.log(data);
    this.setState(_.assign(this.state, {data})); 
  }

  render() {
    const { data, order, orderBy } = this.state;

    return (
      <Paper>
        <Table >
          <EnhancedTableHead 
          onRequestSort={this.handleRequestSort}
          order={order}
          orderBy={orderBy}
          />
          <TableBody>
            {data.map((user: IUser) => {
              return (
                <TableRow key={user.login} onClick={this.props.onRowClick(user)}>
                  <TableCell>{this.getRole(user.role)}</TableCell>
                  <TableCell>{user.login}</TableCell>
                  <TableCell>{user.fullName()}</TableCell>
                  <TableCell>{user.age}</TableCell>
                  <TableCell numeric>{this.getTime(user.registred_on)}</TableCell>
                  <TableCell>{user.active ? "Yes" : "No"}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }

}



