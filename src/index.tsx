import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
var injectTapEventPlugin = require("react-tap-event-plugin");

ReactDOM.render(
  <App title="Таблица пользователей до 55 лет"/>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
injectTapEventPlugin();
