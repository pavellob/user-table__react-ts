import * as React from 'react';
import * as _ from 'lodash';
import { IUser } from "../../models/User";
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import UserCard from '../../components/UserCard/UserCard';
import UsersTable from '../../components/UsersTable/UsersTable';
import './App.css';

const logo = require('./assets/logo.svg');

interface Properties  {
  title: string
}

const newUser = {
  role: 4,
  active: true,
  age: 18,
};

const users = [{
  first_name: "Pavel",
  last_name: "Lobachev",
  role: 1,
  password: 'dfdsfdsfdsfds',
  login: "pavellob",
  active: true,
  age: 30,
  registred_on: 1508798437345,
},
{
  first_name: "Boris",
  last_name: "Britva",
  role: 2,
  password: 'dfdsfdsfdsfds',
  login: "britvass",
  active: true,
  age: 50,
  registred_on: 1508798007315

},
{
  first_name: "Ivan",
  last_name: "Petrov",
  role: 3,
  password: 'dfdsfdsfdsfds',
  login: "petrov22",
  active: false,
  age: 51,
  registred_on: 1508791037340,
}];

class App extends React.Component<Properties, any> {
  constructor(props?: Properties, context?: any) {
    super(props, context);
    this.state = {
      activeUser: null,
      users: users,
    };
  }

  // метод - конечная точка для прототипа - здесь  эмулируем отправку на сервер - пишем пользователю registred_on и возращаем массив ошибок, например, на дубль login
  saveUserCard = (user: IUser) => {
    const newState = _.clone(this.state);
    user.register();
    const index = _.findIndex(newState.users, {registred_on: user.registred_on});
    if(index !== -1) {
      newState.users.splice(index, 1, user);
    } else {
      //отсылаем по api, в случае дубля по ключам - венет ошибку, тут проверяем ручками
      if(_.find(newState.users, {login: user.login})) {
        //возвращаем ошибку
        return [{property: 'login', constraints: {duble: `Login alredy in use`}}];
      } else {
        newState.users.push(user);
      }
    }
    this.setState(newState);
    return [];
  }

  closeUserCard =()=> {
    const newState = _.merge(this.state, {activeUser: null});
    this.setState(newState);
  }


  addUserCard = ()=> {
    const newState = _.merge(this.state, {activeUser: null});
    this.setState(newState, ()=> {
      const newState = _.merge(this.state, {activeUser: newUser});
      this.setState(newState);
    });
  }

  editUserCard = (user: IUser) => ()=> {
    const newState = _.merge(this.state, {activeUser: null});
    this.setState(newState, ()=> {
      const newState = _.merge(this.state, {activeUser: user});
      this.setState(newState);
    });
  }

  renderUserCard() {
    const card = this.state.activeUser ? <UserCard user={this.state.activeUser} cancelMethod={this.closeUserCard} successMethod={this.saveUserCard}/> : null;
    return card;
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <img src="https://media.giphy.com/media/11TrIqHB9jLFJu/giphy.gif" alt="HTML5 Icon" className="App-header__gif"/>
          <h2>{this.props.title}</h2>
        </div>
        <div className="App__add-btn">
          <Button fab color="primary" aria-label="add" onClick={this.addUserCard}>
            <AddIcon />
          </Button>
        </div>
        <div className="App-container">
          <div className="App__user-table">
            <UsersTable users={this.state.users} onRowClick={this.editUserCard}/>
          </div>
          <div className="App__user-card">
            {this.renderUserCard()}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
